    /*******************************************************************************
     * @function 获取某个年某个月中所有周的开始日期与最后日期，周日为第一天，周六为最后一天
     * @author 	chenzhiwei
     * @date 	2019-09-16
    /******************************************************************************/
    function getWeeks($year,$month){
        $dayInWeek = date("w",mktime(0,0,0,$month,1,$year));
        $firstDayOfWeek = date("Y-m-d",strtotime("$year-$month-1 - $dayInWeek days"));
        $endDayOfWeek = date("Y-m-d",strtotime($firstDayOfWeek." +6 days"));
        $dateRange[] = [$firstDayOfWeek,$endDayOfWeek];
        for ($i=0;$i<6;$i++){
            if($month != date("m",strtotime($firstDayOfWeek." +7 days"))){
                break;
            }
            $firstDayOfWeek = date("Y-m-d",strtotime($firstDayOfWeek." +7 days"));
            $endDayOfWeek = date("Y-m-d",strtotime($endDayOfWeek." +7 days"));
            $dateRange[] = [
                $firstDayOfWeek,
                $endDayOfWeek
            ];
        }
        return $dateRange;
    }